﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace CVMAwards.Infrastructure.Repository
{
    public class RepositoryAsync<T> : IRepositoryAsync<T> where T : class
    {
        protected readonly DAL.AwardsDbContext db;

        public RepositoryAsync(DAL.AwardsDbContext context)
        {
            db = context;
        }

        public async Task<int> AddAsync(T t)
        {
            await db.Set<T>().AddAsync(t);
            int i = await new UnitOfWorkAsync(db).Complete();
            return i;
        }

        public async Task<int> SaveAsync(T t)
        {
            db.Entry(t).State = EntityState.Modified;
            int i = 0;
            try
            {
                i = await new UnitOfWorkAsync(db).Complete();
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return i;

        }
        public async void AddRangeAsync(IEnumerable<T> entities)
        {
            await db.Set<T>().AddRangeAsync(entities);
            int i = await new UnitOfWorkAsync(db).Complete();
        }

        public async Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicate)
        {
            return await db.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<T> GetAsync(int id)
        {
            return await db.Set<T>().FindAsync(id);
        }

        public async Task<T> GetAsync(string id)
        {
            return await db.Set<T>().FindAsync(id);
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await db.Set<T>().ToListAsync();
        }

        public async void RemoveAsync(T t)
        {
            db.Set<T>().Remove(t);
            int i = await new UnitOfWorkAsync(db).Complete();
        }

        public async void RemoveRangeAsync(IEnumerable<T> entities)
        {
            db.Set<T>().RemoveRange(entities);
            int i = await new UnitOfWorkAsync(db).Complete();
        }

        //public void SaveAudit(string userName)
        //{
        //    new AuditHelper(db).AddAuditLogs(userName);            
        //}
    }//end class RepositoryAsync

    public class UnitOfWorkAsync : IUnitOfWorkAsync
    {
        private readonly DAL.AwardsDbContext db;

        public UnitOfWorkAsync(DAL.AwardsDbContext context)
        {
            db = context;
        }
        public async Task<int> Complete()
        {
            await db.SaveChangesAsync(true);
            return 1;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
