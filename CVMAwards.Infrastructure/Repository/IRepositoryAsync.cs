﻿using System.Linq.Expressions;

namespace CVMAwards.Infrastructure.Repository
{
    public interface IRepositoryAsync<T> where T : class
    {
        Task<T> GetAsync(int id);
        Task<T> GetAsync(string id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicate);
        Task<int> AddAsync(T t);
        void AddRangeAsync(IEnumerable<T> entities);
        void RemoveAsync(T t);
        void RemoveRangeAsync(IEnumerable<T> entities);
        Task<int> SaveAsync(T t);
        //void SaveAudit(string userName);
    }

    public interface IUnitOfWorkAsync : IDisposable
    {
        Task<int> Complete();
    }
}
