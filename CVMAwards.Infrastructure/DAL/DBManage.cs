﻿using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace CVMAwards.Infrastructure.DAL
{
    public class DBManage
    {
        private static SqlParameter mParameter;
        private static IConfiguration configuration;

        public DBManage()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            configuration = builder.Build();
        }
        private static string GetConnectionString()
        {
            return configuration.GetConnectionString("AwardsDBConnection");
        }

        public static void OpenConnection(SqlConnection connection)
        {
            connection.ConnectionString = GetConnectionString();
            connection.Open();
        }

        private static SqlDbType getParameterType(string mType)
        {
            switch (mType.ToUpper())
            {
                case "INT32":
                    return SqlDbType.Int;
                case "SINGLE":
                    return SqlDbType.Float;
                case "STRING":
                    return SqlDbType.VarChar;
                case "DATETIME":
                    return SqlDbType.DateTime;
                case "XMLDOCUMENT":
                    return SqlDbType.Xml;
                case "IMAGE":
                    return SqlDbType.Image;
                case "BOOLEAN":
                    return SqlDbType.Bit;
                default:
                    return SqlDbType.VarChar;
            }
        }

        public static SqlParameter setInputParameter<T>(string mParaName, T mParaValue, int mParaSize)
        {
            SqlDbType Type = getParameterType(mParaValue.GetType().Name);
            mParameter = new SqlParameter("@" + mParaName, Type, mParaSize);
            mParameter.Direction = ParameterDirection.Input;
            mParameter.Value = mParaValue;

            return mParameter;
        }

        public static SqlParameter setOutputParameter(string mParaName, string mParaType, int mParaSize)
        {
            SqlDbType Type = getParameterType(mParaType);
            switch (Type)
            {
                case SqlDbType.Int:
                    mParameter = new SqlParameter("@" + mParaName, Type);
                    break;
                case SqlDbType.Float:
                    mParameter = new SqlParameter("@" + mParaName, Type);
                    break;
                default:
                    mParameter = new SqlParameter("@" + mParaName, Type, mParaSize);
                    break;
            }
            mParameter.Direction = ParameterDirection.Output;

            return mParameter;
        }

        public int ExecuteNonQuery(string SPName, SqlParameter[] input)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                OpenConnection(connection);
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandTimeout = 0;

                    cmd.CommandText = SPName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (!(input == null))
                        cmd.Parameters.AddRange(input);

                    SqlParameter retval = cmd.Parameters.Add("ReturnValue", SqlDbType.Int);
                    retval.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();

                    return 0;
                }
            }

        }

        public SqlCommand ExecuteNonQuery(string SPName, SqlParameter[] input, SqlParameter[] output)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                OpenConnection(connection);
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandTimeout = 0;

                    cmd.CommandText = SPName;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (!(input == null))
                        cmd.Parameters.AddRange(input);

                    if (!(output == null))
                        cmd.Parameters.AddRange(output);

                    cmd.ExecuteNonQuery();

                    return cmd;
                }
            }

        }
        public DataTable GetDataTable(string SPName, SqlParameter[] input)
        {
            DataTable dt = new DataTable();
            using (SqlConnection connection = new SqlConnection())
            {
                OpenConnection(connection);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                cmd.CommandTimeout = 0;

                cmd.CommandText = SPName;
                cmd.CommandType = CommandType.StoredProcedure;
                if (!(input == null))
                    cmd.Parameters.AddRange(input);

                SqlDataAdapter adapter = new SqlDataAdapter();

                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                connection.Close();
            }

            return dt;
        }

        public DataSet GetDataSet(string SPName, SqlParameter[] input)
        {
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection())
            {
                OpenConnection(connection);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                cmd.CommandTimeout = 0;

                cmd.CommandText = SPName;
                cmd.CommandType = CommandType.StoredProcedure;
                if (!(input == null))
                    cmd.Parameters.AddRange(input);

                SqlDataAdapter adapter = new SqlDataAdapter();

                adapter.SelectCommand = cmd;
                adapter.Fill(ds);
            }

            return ds;
        }
    }//end class
}//end namespace

