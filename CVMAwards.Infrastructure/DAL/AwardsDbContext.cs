﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using CVMAwards.Core.Account;
using CVMAwards.Core.Entities;
using CVMAwards.Core.Views;

namespace CVMAwards.Infrastructure.DAL
{
    public class AwardsDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public AwardsDbContext(DbContextOptions<AwardsDbContext> options) : base(options)
        {

        }

        public DbSet<ApplicationUser> AspNetUsers { get; set; }
        public DbSet<Award> Awards { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Eligibility> Eligibilities { get; set; }
        public DbSet<MasterCode> MasterCodes { get; set; }
        public DbSet<AwardsHomeView> AwardsHomeViews { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Audit> Audits { get; set; }
        public DbSet<Nominee> Nominees { get; set; }


    }//end class
}//end namespace
