﻿using Microsoft.Extensions.Configuration;

namespace CVMAwards.Infrastructure.Interfaces
{
    public interface IAppConfigManager
    {
        string DBConnection { get; }

        string EmailID { get; }

        string AccountKey { get; }

        string GetConnectionString(string connectionName);

        IConfigurationSection GetConfigurationSection(string Key);
    }
}
