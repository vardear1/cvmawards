﻿using Microsoft.Extensions.Configuration;

namespace CVMAwards.Infrastructure.Interfaces
{
    public class AppConfigManager : IAppConfigManager
    {
        private readonly IConfiguration _configuration;
        public AppConfigManager(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public string DBConnection => _configuration["ConnectionStrings:ResourceDBConnection"];

        public string GetConnectionString(string connectionName)
        {
            return this._configuration.GetConnectionString(connectionName);
        }
        public string EmailID => this._configuration["AppSeettings:EmailID"];

        public string AccountKey => this._configuration["AppSeettings:AccountKey"];

        public IConfigurationSection GetConfigurationSection(string key)
        {
            return this._configuration.GetSection(key);
        }
    }
}
