﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Core.Account
{
    public class ApplicationUser : IdentityUser
    {
        [Required(ErrorMessage = "User Name is required")]
        public override string? UserName { get => base.UserName; set => base.UserName = value; }
        [Required(ErrorMessage = "First Name is required")]
        public virtual string? FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        public virtual string? LastName { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Email format is not correct")]
        public override string? Email { get => base.Email; set => base.Email = value; }
        public virtual bool IsDomainUser { get; set; }
        public virtual bool IsActive { get; set; }
    }

    public class ApplicationRole : IdentityRole//<string>
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }
        [Required(ErrorMessage = "Role Name is required")]
        public override string Name { get => base.Name; set => base.Name = value; }
        public string Description { get; set; }
    }
}
