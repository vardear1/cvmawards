﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CVMAwards.Core.Views
{
    [Table("vwAwardsList")]
    public class AwardsHomeView
    {
        [Key]
        public virtual int AwardId { get; set; }
        public virtual string? AwardName { get; set; }
        public virtual string? AwardUrl { get; set; }
        public virtual string? Description { get; set; }
        public virtual string? Requirements { get; set; }
        public virtual string? AwardingBody { get; set; }
        public virtual string? Liaison { get; set; }
        public virtual string? CategoryName { get; set; }
        public virtual string? Eligibility { get; set; }
        public virtual string? Status { get; set; }
        public virtual string? Deadline { get; set; }
        public bool CanNominate { get; set; }
        public int NumOfNominees { get; set; }
        public bool IsActive { get; set; }
    }
}
