﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace CVMAwards.Core.Entities
{
    public class Award
    {
        [Key]
        public virtual int AwardId { get; set; }
        [Display(Name = "Name of Award")]
        [Required(ErrorMessage = "Award name is required")]
        [MaxLength(125, ErrorMessage = "Maximum 125 characters allowed")]
        public virtual string? AwardName { get; set; }
        [DataType(DataType.Url, ErrorMessage = "Enter a valid url")]
        [MaxLength(1000, ErrorMessage = "Maximum 1000 characters allowed")]
        public virtual string? AwardUrl { get; set; }
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        [MaxLength(8000, ErrorMessage = "Maximum 8000 characters allowed")]
        public virtual string? Description { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(8000, ErrorMessage = "Maximum 8000 characters allowed")]
        [AllowHtml]
        public virtual string? Requirements { get; set; }
        [MaxLength(200, ErrorMessage = "Maximum 200 characters allowed")]
        public virtual string? AwardingBody { get; set; }
        public virtual bool LiaisonNeeded { get; set; }
        [ForeignKey("Category")]
        public virtual int? CategoryId { get; set; }
        //[ForeignKey("Eligibility")]
        //public virtual int? EligibilityId{ get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(8000, ErrorMessage = "Maximum 8000 characters allowed")]
        [AllowHtml]
        public virtual string? Eligibility { get; set; }
        public virtual int? Status { get; set; }
        [DataType(DataType.Date)]
        public virtual DateTime? Deadline { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(8000, ErrorMessage = "Maximum 8000 characters allowed")]
        [AllowHtml]
        public virtual string? Notes { get; set; }
        [DataType(DataType.MultilineText)]
        [MaxLength(8000, ErrorMessage = "Maximum 8000 characters allowed")]
        [AllowHtml]
        public virtual string? AdditionalLinks { get; set; }
        [MaxLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public virtual string? Liaison { get; set; }
        public virtual int? DeadlineMonth { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
