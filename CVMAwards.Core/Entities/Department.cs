﻿using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Core.Entities
{
    public class Department
    {
        [Key]
        public virtual int DepartmentId { get; set; }
        public virtual string ShortTitle { get; set; }
        public virtual string LongTitle { get; set; }
        public virtual bool Active { get; set; }

    }
}
