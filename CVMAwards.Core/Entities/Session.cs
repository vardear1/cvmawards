﻿namespace CVMAwards.Core.Entities
{
    public class Session
    {
        public virtual int SessionId { get; set; }
        public virtual int AwardId { get; set; }
        public virtual DateTime? Deadline { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
