﻿using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Core.Entities
{
    public class MasterCode
    {
        [Key]
        [ScaffoldColumn(false)]
        public virtual int CodeId { get; set; }
        [Display(Name = "Code Type")]
        [Required(ErrorMessage = "Code Type is required")]
        public virtual string CodeType { get; set; }
        [Required(ErrorMessage = "Code is required")]
        public virtual string Code { get; set; }
        [Display(Name = "Code Description")]
        [Required(ErrorMessage = "Code Description is required")]
        public virtual string CodeDesc { get; set; }
        [Display(Name = "Active")]
        public virtual bool IsActive { get; set; }
    }
}
