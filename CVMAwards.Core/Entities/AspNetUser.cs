﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Core.Entities
{
    public class AspNetUser
    {
        [Key]
        public virtual string? Id { get; set; }
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "User Name is required")]
        public virtual string? UserName { get; set; }
        public virtual string? NormalizedUserName { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        public virtual string? Email { get; set; }
        public virtual string? NormalizedEmail { get; set; }
        public virtual bool EmailConfirmed { get; set; }
        public virtual string? PasswordHash { get; set; }
        public virtual string? SecurityStamp { get; set; }
        public virtual string? ConcurrencyStamp { get; set; }
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public virtual string? PhoneNumber { get; set; }
        public virtual bool PhoneNumberConfirmed { get; set; }
        public virtual bool TwoFactorEnabled { get; set; }
        public virtual DateTime? LockoutEnd { get; set; }
        public bool LockoutEnabled { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required")]
        public virtual string? FirstName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required")]
        public virtual string? LastName { get; set; }
        [Display(Name = "Domain User?")]
        public virtual bool IsDomainUser { get; set; }
        [Display(Name = "Is Active?")]
        public virtual bool IsActive { get; set; }
        public virtual int AccessFailedCount { get; set; }
    }
}

