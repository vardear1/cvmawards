﻿namespace CVMAwards.Core.Entities
{
    public class Nominee
    {
        public int NomineeId { get; set; }
        public int AwardId { get; set; }
        public string? Name { get; set; }
        public string? NominatedBy { get; set; }
        public DateTime NominatedOn { get; set; }
    }
}
