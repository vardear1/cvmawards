﻿using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Core.Entities
{
    public class AspNetRole
    {
        [Key]
        public virtual string Id { get; set; }
        [Display(Name = "Role Name")]
        [Required(ErrorMessage = "Role Name is required")]
        public virtual string Name { get; set; }
        public virtual string NormalizedName { get; set; }
        public virtual string ConcurrencyStamp { get; set; }
        public string Description { get; set; }
    }

    public class AspNetUserRole
    {
        [Key]
        public virtual string UserId { get; set; }
        public virtual string RoleId { get; set; }
    }
}
