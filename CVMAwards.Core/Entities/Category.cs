﻿using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Core.Entities
{
    public class Category
    {
        [Key]
        public virtual int CategoryId { get; set; }
        public virtual string CategoryName { get; set; }
        public virtual bool IsActive { get; set; }
    }

    public class Eligibility
    {
        [Key]
        public virtual int EligibilityId { get; set; }
        public virtual string Description { get; set; }
        public virtual bool? IsActive { get; set; }
    }
}
