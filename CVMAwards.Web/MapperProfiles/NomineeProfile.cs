﻿using AutoMapper;
using CVMAwards.Core.Entities;
using CVMAwards.Web.ViewModels.Nominee;

namespace CVMAwards.Web.MapperProfiles
{
    public class NomineeProfile : Profile
    {
        public NomineeProfile()
        {
            CreateMap<Nominee, NomineeViewModel>();
            CreateMap<NomineeViewModel, Nominee>();
        }
    }
}
