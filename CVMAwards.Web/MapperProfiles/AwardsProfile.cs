﻿using AutoMapper;
using CVMAwards.Core.Entities;
using CVMAwards.Web.Models.Home;
using CVMAwards.Web.ViewModels.Awards;
using CVMAwards.Web.ViewModels.Home;

namespace CVMAwards.Web.MapperProfiles
{
    public class AwardsProfile : Profile
    {
        public AwardsProfile()
        {
            CreateMap<Award, AwardHomeViewModel>();
            CreateMap<AwardHomeViewModel, Award>();
            CreateMap<AwardDTO, Award>();
            CreateMap<Award, AwardDTO>();
            CreateMap<AwardsViewModel, Award>();
            CreateMap<Award, AwardsViewModel>();
            CreateMap<AwardsViewModel, AwardsViewModel>();
            CreateMap<Award, Award>();
        }
    }
}
