﻿using AutoMapper;
using CVMAwards.Core.Account;
using CVMAwards.Core.Entities;
using CVMAwards.Web.Models.Users;
using CVMAwards.Web.ViewModels.Account;

namespace CVMAwards.Web.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<AspNetUser, AspNetUserViewModel>();
            CreateMap<RegisterViewModel, ApplicationUser>();
        }
    }
}
