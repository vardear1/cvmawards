using AutoMapper;
using CVMAwards.Core.Account;
using CVMAwards.Core.Entities;
using CVMAwards.Infrastructure.DAL;
using CVMAwards.Infrastructure.Interfaces;
using CVMAwards.Infrastructure.Repository;
using CVMAwards.Web.Models.Common;
using CVMAwards.Web.Models.Home;
using CVMAwards.Web.Services;
using CVMAwards.Web.ViewModels.Awards;
using CVMAwards.Web.ViewModels.Home;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Web;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

var builder = WebApplication.CreateBuilder(args);

ConfigurationManager configuration = builder.Configuration;
builder.Services.Configure<CookieTempDataProviderOptions>(options => { options.Cookie.IsEssential = true; });
builder.Services.AddMediatR(typeof(Program)); //.GetTypeInfo().Assembly);
var connString = builder.Configuration.GetConnectionString("AwardsDBConnection");
builder.Services.AddDbContextPool<AwardsDbContext>(options => {
    options.UseSqlServer(connString);
    options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
}); 

builder.Services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
{
    options.Password.RequiredLength = 8;
    options.User.RequireUniqueEmail = true;
    options.Password.RequireLowercase = true;
    options.Password.RequireUppercase = true;
    options.Password.RequireDigit = true;
}).AddEntityFrameworkStores<AwardsDbContext>();

builder.Services.AddScoped(typeof(IRepositoryAsync<>), typeof(RepositoryAsync<>));

// Add services to the container.
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddSingleton<IAppConfigManager, AppConfigManager>();
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options => options.Cookie.SameSite = SameSiteMode.None);

builder.Services.Configure<AppSettingsModel>(configuration.GetSection("AppSettings"));
var section = configuration.GetSection("AppSettings");
var baseAPIUrl = section.GetValue<string>("BaseAPIURL");
builder.Services.AddScoped<IAuthAPIService, AuthAPIService>();
builder.Services.AddHttpClient<IAuthAPIService, AuthAPIService>(client =>
{
    client.BaseAddress = new Uri(baseAPIUrl);
});

//var config = new MapperConfiguration(cfg =>
//{
//    cfg.CreateMap<Award, AwardHomeViewModel>();
//    cfg.CreateMap<AwardHomeViewModel, Award>();
//    cfg.CreateMap<AwardDTO, Award>();
//    cfg.CreateMap<Award, AwardDTO>();
//    cfg.CreateMap<AwardsViewModel, Award>();
//    cfg.CreateMap<Award, AwardsViewModel>();
//    cfg.CreateMap<AwardsViewModel, AwardsViewModel>();
//});

//var mapper = config.CreateMapper();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Logging.ClearProviders();
builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Error);
builder.Host.UseNLog();

builder.Services.AddSession(options => {
    options.IdleTimeout = TimeSpan.FromMinutes(15);//You can set Time   
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();
NLog.GlobalDiagnosticsContext.Set("connectionString", connString);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
