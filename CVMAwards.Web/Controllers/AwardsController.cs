﻿using AutoMapper;
using CVMAwards.Web.Extensions;
using CVMAwards.Web.Models.Common;
using CVMAwards.Web.ViewModels.Awards;
using CVMAwards.Web.ViewModels.Home;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CVMAwards.Web.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    public class AwardsController : Controller
    {
        private readonly IMediator _mediator;
        private readonly ILogger<AwardsController> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        readonly IMapper _mapper;
        private readonly ISession _session;
        public AwardsController(IMediator mediator,
            ILogger<AwardsController> logger,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
            //_session = _httpContextAccessor.HttpContext.Session;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetAwardsList()
        {

            JsonResult result = new JsonResult(null);

            string draw = Request.Form["draw"][0];

            var parms = new TableParams();

            var formData = Request.Form;


            parms.Length = Convert.ToInt32(formData["length"].ToString());
            parms.Start = Convert.ToInt32(formData["start"].ToString());
            parms.ColIndex = Convert.ToInt32(formData["order[0][column]"].ToString());
            parms.SortDir = formData["order[0][dir]"].ToString();
            parms.Search = formData["search[value]"].ToString();
            //}            

            var queryresult = new AwardHomeViewModel().GetAwardsList(parms, "awards", true);
            //var queryresult = _mediator.SendAsync(query);
            //_session.SetObject("params", parms);

            //parms = _session.GetObject<TableParams>("params");
            result = Json(new
            {
                draw = Convert.ToInt32(draw),
                recordsTotal = queryresult.TotalRecords,
                recordsFiltered = queryresult.RecordCount,
                data = queryresult.AwardsList
            });
            return result;
        }

        public async Task<IActionResult> AddEdit(int id = 0)
        {
            var request = new AwardsViewModel.AwardsViewQuery();
            request.AwardId = id;

            var response = await _mediator.Send(request);
            //return PartialView("_AddEdit", response);
            return View("AddEdit", response);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEdit(AwardsViewModel viewModel, CancellationToken token)
        {
            var request = new AwardsViewModel.AwardsViewQuery();
            request.AwardId = viewModel.AwardId;

            var response = await _mediator.Send(request);
            viewModel.Categories = response.Categories;
            viewModel.Months = response.Months;

            if (!ModelState.IsValid)
            {
                return View(viewModel);

            }

            var command = new AwardsViewModel.AwardsViewCommand(viewModel);
            var result = await _mediator.Send(command, token);

            if (result.Success)
            {
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "There was an error saving the award");
            ModelState.AddModelError("", result.Errors.First());
            return View(viewModel);
            
        }
    }//end class AwardsController
}//end namespace
