﻿using AutoMapper;
using CVMAwards.Core.Account;
using CVMAwards.Web.Models.LDAP;
using CVMAwards.Web.Services;
using CVMAwards.Web.ViewModels.Account;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CVMAwards.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        private readonly IAuthAPIService _service;
        private readonly RoleManager<ApplicationRole> _roleManager;
        readonly IMapper _mapper;
        public AccountController(UserManager<ApplicationUser> userManager,
                SignInManager<ApplicationUser> signInManager,
                RoleManager<ApplicationRole> roleManager,
                IMediator mediator,
                IConfiguration configuration,
                IAuthAPIService service,
                IMapper mapper
            )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            _roleManager = roleManager;
            _mediator = mediator;
            _configuration = configuration;
            _service = service;
            _mapper = mapper;
        }
        public IActionResult Login(string? returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            bool isValidated;

            var user = await userManager.FindByNameAsync(model.UserName);

            if (user == null)
            {
                var query = new LDAPQueryModel()
                {
                    ShowExistingUsers = false,
                    UserFilter = model.UserName,
                    FilteredGroup = "SelectAGroup"
                };

                var ldapUsers = await _service.GetLDAPUsers(query);

                if (ldapUsers.Users == null || ldapUsers.Users.Count == 0)
                {
                    ModelState.AddModelError("", "You do not have a CVM account. Please contact CVM IT for assistance.");
                    return View(model);
                }

                var ldapuser = ldapUsers.Users[0];

                var isError = false;
                if (string.IsNullOrEmpty(ldapuser.FirstName))
                {
                    ModelState.AddModelError("", "First name is missing in the CVM AD. Please contact the CVM IT to update this");
                    isError = true;
                }
                if (string.IsNullOrEmpty(ldapuser.LastName))
                {
                    ModelState.AddModelError("", "Last name is missing in the CVM AD. Please contact the CVM IT to update this");
                    isError = true;
                }
                if (string.IsNullOrEmpty(ldapuser.Email))
                {
                    ModelState.AddModelError("", "Email is missing in the CVM AD. Please contact the CVM IT to update this");
                    isError = true;
                }

                if (isError)
                {
                    return View(model);
                }

                user = new ApplicationUser()
                {
                    UserName = model.UserName,
                    FirstName = ldapuser.FirstName,
                    LastName = ldapuser.LastName,
                    Email = ldapuser.Email,
                    IsDomainUser = true,
                    IsActive = true,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = false
                };

                var result = await userManager.CreateAsync(user);

                if (result.Succeeded)
                {
                    result = await userManager.AddToRoleAsync(user, "User");
                }
            }

            isValidated = await _service.Login(model);

            if (isValidated)
            {
                await signInManager.SignInAsync(user, model.RememberMe);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                model.UseCampus = true;
                isValidated = await _service.Login(model);
                if (isValidated)
                {
                    await signInManager.SignInAsync(user, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }

                ModelState.AddModelError("", "Invalid user name or password");
            }

            return View(model);
        }//end action login

        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, CancellationToken token)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                var user = await userManager.FindByNameAsync(model.UserName);
                if (user != null)
                {
                    ModelState.AddModelError("", "User name already exists");
                }
                
                user = await userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    {
                        ModelState.AddModelError("", "Email already exists");
                    }
                }
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                if (user  == null)
                {
                    user = new ApplicationUser();
                }
                user = _mapper.Map<ApplicationUser>(model);
                user.IsActive = true;
                user.IsDomainUser = false;
                var result = await userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    result = await userManager.AddToRoleAsync(user, "User");
                    return RedirectToAction("Login");
                }

                AddErrors(result);

            }
            catch (Exception ex)
            {
                string message = ex.Message;
                ModelState.AddModelError("", "Error registering user");
            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        #region Helpers
        private const string XsrfKey = "XsrfId";
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        #endregion
    }
}
