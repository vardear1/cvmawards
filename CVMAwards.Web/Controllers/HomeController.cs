﻿using CVMAwards.Web.Models;
using CVMAwards.Web.Models.Common;
using CVMAwards.Web.ViewModels.Home;
using CVMAwards.Web.ViewModels.Nominee;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CVMAwards.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly IHttpContextAccessor _contextAccessor;
        readonly IMediator _mediator;
        public HomeController(ILogger<HomeController> logger,
            IHttpContextAccessor contextAccessor,
            IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
            _contextAccessor = contextAccessor;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetAwardsList()
        {
            JsonResult result = new JsonResult(null);

            //var query = new AwardHomeViewModel.AwardHomeQuery();
            //query.Id = 0;
            //query.Params = new TableParams();

            string draw = Request.Form["draw"][0];
            var formData = Request.Form;

            var parms = new TableParams();
            parms.Length = Convert.ToInt32(formData["length"].ToString());
            parms.Start = Convert.ToInt32(formData["start"].ToString());
            parms.ColIndex = Convert.ToInt32(formData["order[0][column]"].ToString());
            parms.SortDir = formData["order[0][dir]"].ToString();
            parms.Search = formData["search[value]"].ToString();
            parms.UserName = HttpContext.User.Identity == null? string.Empty : HttpContext.User.Identity.Name;
            var queryresult = new AwardHomeViewModel().GetAwardsList(parms, "home", true);
            //var queryresult = _mediator.SendAsync(query);            

            result = Json(new
            {
                draw = Convert.ToInt32(draw),
                recordsTotal = queryresult.TotalRecords,
                recordsFiltered = queryresult.RecordCount,
                data = queryresult.AwardsList
            });
            return result;
        }

        public async Task<IActionResult> Details(int id)
        {
            var request = new AwardDetailViewModel.AwardsViewQuery();
            request.AwardId = id;

            var response = await _mediator.Send(request);
            return PartialView("_Detail", response);
        }

        public async Task<IActionResult> AddNominee(int id = 0)
        {
            var request = new NomineeViewModel.NomineeQuery();
            request.AwardId = id;

            var response = await _mediator.Send(request);
            return PartialView("_AddNominee", response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddNominee(NomineeViewModel model, CancellationToken token)
        {
            object obj = JsonResponseFactory.SuccessResponse();

            try
            {
                var command = new NomineeViewModel.NomineeCommand(model);
                await _mediator.Send(command, token);
                //return RedirectToAction("Index");
                obj = JsonResponseFactory.SuccessResponse("Nominee has been saved successfully");
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("Nominated"))
                {
                    obj = JsonResponseFactory.ErrorResponse("Another user has nominated someone for this award. Please refresh your browser to get updated data.");
                }
                else
                {
                    obj = JsonResponseFactory.ErrorResponse("There was an error saving the Nominee");
                }
                _logger.LogError(ex, ex.Message, null);
                obj = JsonResponseFactory.ErrorResponse("There was an error saving the Nominee");
            }

            return Json(obj);
        }

        public async Task<IActionResult> ViewNominee(int id)
        {
            var request = new NomineeViewModel.NomineeQuery();
            request.AwardId = id;

            var response = await _mediator.Send(request);
            return PartialView("_ViewNominee", response);
        }
        public async Task<IActionResult> GetDescription(int id)
        {
            var request = new AwardDetailViewModel.AwardsViewQuery();
            request.AwardId = id;

            var response = await _mediator.Send(request);

            object obj = new { description = response.DTO.Description };


            return Json(obj);
        }

        public async Task<IActionResult> GetRequirements(int id)
        {
            var request = new AwardDetailViewModel.AwardsViewQuery();
            request.AwardId = id;

            var response = await _mediator.Send(request);

            object obj = new { requirements = response.DTO.Requirements };


            return Json(obj);
        }

        public async Task<IActionResult> GetEligibility(int id)
        {
            var request = new AwardDetailViewModel.AwardsViewQuery();
            request.AwardId = id;

            var response = await _mediator.Send(request);

            object obj = new { eligibility = response.DTO.Eligibility };


            return Json(obj);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}