﻿using CVMAwards.Core.Account;
using CVMAwards.Web.Extensions;
using CVMAwards.Web.Models.LDAP;
using CVMAwards.Web.Models.Users;
using CVMAwards.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CVMAwards.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly IMediator _mediator;
        private readonly ILogger<UsersController> _logger;
        private IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        //private readonly IMapper _mapper;
        readonly IAuthAPIService _service;

        public UsersController(IMediator mediator,
            ILogger<UsersController> logger,
            IHttpContextAccessor httpContextAccessor,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IAuthAPIService service)
        {
            _mediator = mediator;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
            _roleManager = roleManager;
            _service = service;
        }
        public async Task<IActionResult> Index()
        {
            var query = new AspNetUserViewModel.AspNetUserViewModelQuery();
            var result = await _mediator.Send(query);
            return View(result);
        }

        public async Task<IActionResult> AddEdit(string id)
        {
            var query = new AspNetUserViewModel.AspNetUserViewModelQuery();

            if (string.IsNullOrEmpty(id))
            {
                query.Type = "Add";
            }
            else
            {
                query.Type = "Edit";
                query.UserId = id;
            }

            var result = await _mediator.Send(query);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddEdit(AspNetUserViewModel command, CancellationToken token)
        {
            var aspNetUserViewModel = new AspNetUserViewModel.AspNetUserViewModelCommand(command);

            var errors = await new AspNetUserViewModel().GetErrors(command, _roleManager, _userManager);

            ModelState.Clear();
            if (errors.Count > 0)
            {
                foreach (var error in errors)
                {
                    ModelState.AddModelError("", error);
                }

                return View(command);
            }

            try
            {
                await _mediator.Send(aspNetUserViewModel, token);
                var result = new LDAPResult();
                result.Success = true;
                result.Errors.Add("User has been saved successfully");
                TempData.Put("LdapImportResult", result);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ModelState.AddModelError("", "There was an error saving the data");
                return View(command);
            }
        }

        public async Task<IActionResult> LdapImport(bool showExistingUsers = false, string userFilter = null, string filteredGroup = "SelectAGroup")
        {
            var query = new LdapUserImportViewModel.LdapUserImportQuery();
            query.ShowExistingUsers = showExistingUsers;
            query.UserFilter = userFilter;
            query.FilteredGroup = filteredGroup;
            var model = await _mediator.Send(query);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LdapImport(LdapUserImportViewModel model)
        {
            if (ModelState.IsValid)
            {
            }

            var query = new LdapUserImportViewModel.LdapUserImportQuery
            {
                ShowExistingUsers = model.ShowExistingUsers,
                UserFilter = model.UserFilter,
                FilteredGroup = model.FilteredGroup
            };

            var r2 = await _mediator.Send(query);

            return View(r2);
        }

        public async Task<IActionResult> LdapImportUser(string Id)
        {
            var query = new LdapUserImportViewModel.LdapUserImportQuery
            {
                ShowExistingUsers = false,
                UserFilter = Id,
                FilteredGroup = "AllUsers"
            };
            var mod = await _mediator.Send(query);

            var query1 = new AspNetUserViewModel.AspNetUserViewModelQuery();
            query1.Type = "Add";

            var result = await _mediator.Send(query1);

            foreach (var user in mod.Users)
            {
                result.User.FirstName = user.FirstName;
                result.User.LastName = user.LastName;
                result.User.Email = user.Email;
                result.User.UserName = user.UserName;
                result.User.IsDomainUser = true;
                result.User.IsActive = true;
            }

            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LdapImportUser(AspNetUserViewModel command, CancellationToken token)
        {
            var result = new LDAPResult();
            var aspNetUserViewModel = new LdapUserImportViewModel.LdapUserImportViewModelCommand(command);
            //try
            //{
            result = await _mediator.Send(aspNetUserViewModel, token);
            if (result.Success)
            {
                result.Errors.Add("User has been imported successfully");
                TempData.Put("LdapImportResult", result);
                return RedirectToAction("LdapImport");
            }
            else
            {
                result.Success = false;
            }

            //TempData.Put("LdapImportResult", result);
            //return RedirectToAction("LdapImport");
            //}
            //catch (Exception ex)
            //{
            //    if (ex.Message.Contains("Role"))
            //    {
            //        result.Success = false;
            //        result.Errors.Add("At least one role must be selected");
            //        TempData.Put("LdapImportResult", result);
            //    }
            //    else //if (ex.Message.Contains("Add") || ex.Message.Contains("Save"))
            //    {
            //        result.Success = false;
            //        result.Errors.Add("There was an error saving the data");
            //        result.Errors.Add(ex.Message);
            //        TempData.Put("LdapImportResult", result);
            //    }
            //    _logger.LogError(ex.Message);
            //}

            var roles = _roleManager.Roles; //_roles.GetAll().ToList();

            foreach (var item in roles)
            {
                foreach (var role in command.RolesList)
                {
                    if (item.Id == role.StringId)
                    {
                        role.Name = item.Name;
                        break;
                    }
                }
            }
            if (!ModelState.IsValid)
            {
                if (ModelState.ContainsKey("Password"))
                {
                    ModelState.Remove("Password");
                }

                if (ModelState.ContainsKey("PasswordConfirm"))
                {
                    ModelState.Remove("PasswordConfirm");
                }

                if (ModelState.ContainsKey("UserRole"))
                {
                    foreach (var role in command.RolesList)
                    {
                        if (role.Checked)
                        {
                            ModelState.Remove("UserRole");
                            break;
                        }
                    }
                }
            }
            return View(command);
        }
    }//end controller
}//end namespace
