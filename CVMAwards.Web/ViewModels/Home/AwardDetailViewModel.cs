﻿using AutoMapper;
using CVMAwards.Core.Entities;
using CVMAwards.Infrastructure.Repository;
using CVMAwards.Web.Models.Common;
using CVMAwards.Web.Models.Home;
using MediatR;

namespace CVMAwards.Web.ViewModels.Home
{
    public class AwardDetailViewModel
    {
        public AwardDTO DTO { get; set; }

        public class AwardsViewQuery : IRequest<AwardDetailViewModel>
        {
            public int AwardId { get; set; }
        }

        public class AwardsViewQueryResult : IRequestHandler<AwardsViewQuery, AwardDetailViewModel>
        {
            private readonly IRepositoryAsync<Award> _award;
            private readonly IRepositoryAsync<Category> _categories;
            readonly IMapper _mapper;
            //private readonly IRepositoryAsync<Session> _sessions;
            public AwardsViewQueryResult(IRepositoryAsync<Award> award,
                IRepositoryAsync<Category> categories,
                IMapper mapper
                //IRepositoryAsync<Session> sessions
                )
            {
                _award = award;
                _categories = categories;
                _mapper = mapper;   
                //_sessions = sessions;
            }
            public async Task<AwardDetailViewModel> Handle(AwardsViewQuery message, CancellationToken token)
            {
                AwardDetailViewModel viewModel = new AwardDetailViewModel();
                var categories = (await _categories.GetAllAsync()).Where(x => x.IsActive == true).OrderBy(x => x.CategoryName).ToList();
                var award = await _award.GetAsync(message.AwardId);

                //if (award.Deadline != null && award.Deadline < DateTime.Now)
                //{
                //    award.Status = 0;
                //    int result = await _award.SaveAsync(award);
                //    var session = (await _sessions.GetAllAsync()).Where(x => x.AwardId == message.AwardId && x.Deadline == award.Deadline).FirstOrDefault();
                //    session.IsActive = false;
                //    result = await _sessions.SaveAsync(session);
                //}

                var dto = _mapper.Map<AwardDTO>(award);
                viewModel.DTO = dto;
                //viewModel.DTO = new AwardDTO() { 
                //    AwardingBody = award.AwardingBody,
                //    AwardName = award.AwardName,
                //    AwardUrl = award.AwardUrl,
                //    Description = award.Description,
                //    Requirements = award.Requirements,
                //    LiaisonNeeded = award.LiaisonNeeded,
                //    Eligibility = award.Eligibility,
                //    Notes = award.Notes,
                //    AdditionalLinks = award.AdditionalLinks
                //};


                viewModel.DTO.StatusDesc = award.Status == 1 ? "Active" : "Inactive";
                //viewModel.DTO.DueDate = award.Deadline == null ? "" : ((DateTime)award.Deadline).ToString("MM/dd/yyyy");
                viewModel.DTO.Month = award.DeadlineMonth == null ? "" : Enum.GetName(typeof(Months), (int)award.DeadlineMonth);
                if (award.CategoryId != null)
                {
                    viewModel.DTO.CategoryDesc = (await _categories.GetAsync((int)award.CategoryId)).CategoryName;
                }
                return viewModel;
            }

        }
    }//end class AwardDetailViewModel
}//end namespace
