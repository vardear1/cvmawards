﻿using CVMAwards.Core.Views;
using CVMAwards.Infrastructure.DAL;
using CVMAwards.Infrastructure.Repository;
using CVMAwards.Web.Models.Common;
using MediatR;
using Microsoft.Data.SqlClient;
using System.Data;

namespace CVMAwards.Web.ViewModels.Home
{
    public class AwardHomeViewModel
    {
        public AwardsHomeView AwardsHomeView { get; set; }
        public List<AwardsHomeView> AwardsList { get; set; }
        public int RecordCount { get; set; }
        public int TotalRecords { get; set; }
        public string AwardDate { get; set; }

        public AwardHomeViewModel GetAwardsList(TableParams Params, string pageFrom, bool IsActive)
        {
            AwardHomeViewModel ViewModel = new AwardHomeViewModel();

            var db = new DBManage();
            List<SqlParameter> input = new List<SqlParameter>();
            ViewModel.RecordCount = 0;
            ViewModel.TotalRecords = 0;

            if (!string.IsNullOrEmpty(Params.Search))
            {
                input.Add(DBManage.setInputParameter<string>("Search", Params.Search, 100));
            }
            input.Add(DBManage.setInputParameter<Int32>("DisplayLength", Params.Length, 10));
            input.Add(DBManage.setInputParameter<Int32>("DisplayStart", Params.Start, 10));
            input.Add(DBManage.setInputParameter<Int32>("SortCol", Params.ColIndex, 10));
            input.Add(DBManage.setInputParameter<string>("SortDir", Params.SortDir, 10));
            if (pageFrom == "home")
            {
                input.Add(DBManage.setInputParameter<Boolean>("IsActive", true, 10));
            }
            //else
            //{
            //    input.Add(DBManage.setInputParameter<Boolean>("IsActive", IsActive, 10));
            //}
            if (!string.IsNullOrEmpty(Params.UserName))
            {
                input.Add(DBManage.setInputParameter<string>("UserName", Params.UserName, 100));
            }
            DataSet ds = db.GetDataSet("GetAwardList", input.ToArray());
            AwardsHomeView model;
            ViewModel.AwardsList = new List<AwardsHomeView>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                model = new AwardsHomeView();
                model.AwardId = Convert.ToInt32(dr["AwardId"]);
                model.AwardName = dr["AwardName"].ToString();
                model.AwardingBody = dr["AwardingBody"].ToString();
                model.AwardUrl = dr["AwardUrl"].ToString();
                model.Description = dr["Description"].ToString();
                model.Requirements = dr["Requirements"].ToString();
                model.CategoryName = dr["CategoryName"].ToString();
                model.Eligibility = dr["Eligibility"].ToString();
                model.Liaison = dr["Liaison"].ToString();
                model.Status = dr["Status"].ToString();
                model.Deadline = dr["Deadline"].ToString();
                model.CanNominate = Convert.ToBoolean(dr["CanNominate"]);
                model.NumOfNominees = Convert.ToInt32(dr["NumOfNominees"]);
                model.IsActive = Convert.ToBoolean(dr["IsActive"]);
                ViewModel.RecordCount = Convert.ToInt32(dr["RecordCount"]);
                ViewModel.AwardsList.Add(model);
            }

            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                ViewModel.TotalRecords = Convert.ToInt32(dr["TotalCount"]);
            }
            return ViewModel;
        }

        public class AwardHomeQuery : IRequest<AwardHomeViewModel>
        {
            public int Id { get; set; }
            public TableParams Params { get; set; }
        }

        public class AwardHomeQueryResult : IRequestHandler<AwardHomeQuery, AwardHomeViewModel>
        {
            private readonly IRepositoryAsync<AwardsHomeView> _awards;
            public AwardHomeQueryResult(IRepositoryAsync<AwardsHomeView> awards)
            {
                _awards = awards;
            }

            public async Task<AwardHomeViewModel> Handle(AwardHomeQuery query, CancellationToken token)
            {
                AwardHomeViewModel ViewModel = new AwardHomeViewModel();

                if (query.Id > 0)
                {
                    ViewModel.AwardsHomeView = (await _awards.FindAsync(x => x.AwardId == query.Id)).FirstOrDefault();
                }
                else
                {
                    var db = new DBManage();
                    List<SqlParameter> input = new List<SqlParameter>();
                    ViewModel.RecordCount = 0;
                    ViewModel.TotalRecords = 0;

                    if (!string.IsNullOrEmpty(query.Params.Search))
                    {
                        input.Add(DBManage.setInputParameter<string>("Search", query.Params.Search, 100));
                    }
                    input.Add(DBManage.setInputParameter<Int32>("DisplayLength", query.Params.Length, 10));
                    input.Add(DBManage.setInputParameter<Int32>("DisplayStart", query.Params.Start, 10));
                    input.Add(DBManage.setInputParameter<Int32>("SortCol", query.Params.ColIndex, 10));
                    input.Add(DBManage.setInputParameter<string>("SortDir", query.Params.SortDir, 10));

                    DataSet ds = db.GetDataSet("GetAwardList", input.ToArray());
                    AwardsHomeView model;
                    ViewModel.AwardsList = new List<AwardsHomeView>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        model = new AwardsHomeView();
                        model.AwardId = Convert.ToInt32(dr["AwardId"]);
                        model.AwardName = dr["AwardName"].ToString();
                        model.AwardingBody = dr["AwardingBody"].ToString();
                        model.AwardUrl = dr["AwardUrl"].ToString();
                        model.Description = dr["Description"].ToString();
                        model.Requirements = dr["Requirements"].ToString();
                        model.CategoryName = dr["CategoryName"].ToString();
                        model.Eligibility = dr["Eligibility"].ToString();
                        model.Liaison = dr["Liaison"].ToString();
                        model.Status = dr["Status"].ToString();
                        model.Deadline = dr["Deadline"].ToString();
                        ViewModel.RecordCount = Convert.ToInt32(dr["RecordCount"]);
                        ViewModel.AwardsList.Add(model);
                    }

                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        ViewModel.TotalRecords = Convert.ToInt32(dr["TotalCount"]);
                    }
                }
                return ViewModel;
            }
        }
    }//end class AwardHomeViewModel
}//end namespace
