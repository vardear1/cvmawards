﻿using CVMAwards.Core.Entities;
using System.Text;

namespace CVMAwards.Web.ViewModels.Awards
{
    public class AwardsCheckChanges
    {
        public string GetChangedColumns(Award Old, Award New)
        {
            var changes = new StringBuilder();
            if ((Old.AwardName == null && New.AwardName != null)
                || (Old.AwardName == null && New.AwardName != null))
            {
                changes.Append("AwardName; ");
            }
            else if (Old.AwardName != null && New.AwardName != null)
            {
                if (!(Old.AwardName.Trim().Equals(New.AwardName.Trim())))
                {
                    changes.Append("AwardName; ");
                }
            }


            if ((Old.AwardUrl == null && New.AwardUrl != null)
                || (Old.AwardUrl == null && New.AwardUrl != null))
            {
                changes.Append("AwardUrl; ");
            }
            else if (Old.AwardUrl != null && New.AwardUrl != null)
            {
                if (!(Old.AwardUrl.Trim().Equals(New.AwardUrl.Trim())))
                {
                    changes.Append("AwardUrl; ");
                }
            }


            if ((Old.Description == null && New.Description != null)
                || (Old.Description != null && New.Description == null)
               )
            {
                changes.Append("Description; ");
            }
            else if (Old.Description != null && New.Description != null)
            {
                if (!(Old.Description.Trim().Equals(New.Description.Trim())))
                {
                    changes.Append("Description; ");
                }
            }

            if ((Old.Requirements != null && New.Requirements == null)
                || (Old.Requirements == null && New.Requirements != null))
            {
                changes.Append("Requirements; ");
            }
            else if (Old.Requirements != null && New.Requirements != null)
            {
                if (!(Old.Requirements.Trim().Equals(New.Requirements.Trim())))
                {
                    changes.Append("Requirements; ");
                }
            }

            if ((Old.AwardingBody == null && New.AwardingBody != null)
                || (Old.AwardingBody != null && New.AwardingBody == null))
            {
                changes.Append("AwardingBody; ");
            }
            else if (Old.AwardingBody != null && New.AwardingBody != null)
            {
                if (!(Old.AwardingBody.Trim().Equals(New.AwardingBody.Trim())))
                {
                    changes.Append("AwardingBody; ");
                }
            }

            if ((Old.Liaison != null && New.Liaison == null)
                || Old.Liaison == null && New.Liaison != null
                )
            {
                changes.Append("Liaison; ");
            }
            else if (Old.Liaison != null && New.Liaison != null)
            {
                if (!(Old.Liaison.Trim()).Equals(New.Liaison.Trim()))
                {
                    changes.Append("Liaison; ");
                }
            }

            if (Old.CategoryId != New.CategoryId)
            {
                changes.Append("CategoryId; ");
            }

            if ((Old.Eligibility == null && New.Eligibility != null)
                || (Old.Eligibility != null && New.Eligibility == null)
                )
            {
                changes.Append("Eligibility; ");
            }
            else if (Old.Eligibility != null && New.Eligibility != null)
            {
                if (!(Old.Eligibility.Trim().Equals(New.Eligibility.Trim())))
                {
                    changes.Append("Eligibility; ");
                }
            }

            //if (Old.Status != New.Status)
            //{
            //    changes.Append("Status; ");
            //}

            if ((Old.Notes == null && New.Notes != null)
                || (Old.Notes != null && New.Notes == null))
            {
                changes.Append("Notes; ");
            }
            else if (Old.Notes != null && New.Notes != null)
            {
                if (!(Old.Notes.Trim().Equals(New.Notes.Trim())))
                {
                    changes.Append("Notes; ");
                }
            }

            if ((Old.AdditionalLinks == null && New.AdditionalLinks != null)
                || (Old.AdditionalLinks != null && New.AdditionalLinks == null))
            {
                changes.Append("AdditionalLinks; ");
            }
            else if (Old.AdditionalLinks != null && New.AdditionalLinks != null)
            {
                if (!(Old.AdditionalLinks.Trim().Equals(New.AdditionalLinks.Trim())))
                {
                    changes.Append("AdditionalLinks; ");
                }
            }

            if (Old.DeadlineMonth != New.DeadlineMonth)
            {
                changes.Append("DeadlineMonth; ");
            }

            return changes.ToString();
        }
    }
}
