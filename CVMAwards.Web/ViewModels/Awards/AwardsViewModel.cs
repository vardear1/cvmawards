﻿using AutoMapper;
using CVMAwards.Core.Entities;
using CVMAwards.Core.Views;
using CVMAwards.Infrastructure.Repository;
using CVMAwards.Web.Models.Common;
using CVMAwards.Web.Models.LDAP;
using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace CVMAwards.Web.ViewModels.Awards
{
    public class AwardsViewModel: Award
    {
        //public List<AwardsHomeView> AwardsList { get; set; }
        public int RecordCount { get; set; }
        public int TotalRecords { get; set; }
        public string? AwardDate { get; set; }
        public SelectList? Categories { get; set; }
        public SelectList? Months { get; set; }
        public SelectList? Statuses { get; set; }

        public class AwardsViewQuery : IRequest<AwardsViewModel>
        {
            public int AwardId { get; set; }
            public TableParams Params { get; set; }
        }

        public class AwardsViewQueryHandler : IRequestHandler<AwardsViewQuery, AwardsViewModel>
        {
            private readonly IRepositoryAsync<Award> _award;
            private readonly IRepositoryAsync<Category> _categories;
            private readonly IRepositoryAsync<Eligibility> _eligibilities;
            private readonly IRepositoryAsync<MasterCode> _mastercodes;
            readonly IMapper _mapper;
            public AwardsViewQueryHandler(IRepositoryAsync<Award> award,
                IRepositoryAsync<Category> categories,
                IRepositoryAsync<Eligibility> eligibilities,
                IRepositoryAsync<MasterCode> mastercodes,
                IMapper mapper)
            {
                _award = award;
                _mastercodes = mastercodes;
                _eligibilities = eligibilities;
                _categories = categories;
                _mapper = mapper;
            }

            public async Task<AwardsViewModel> Handle(AwardsViewQuery query, CancellationToken cancellationToken)
            {
                AwardsViewModel viewModel = new AwardsViewModel();
                
                if (query.AwardId == 0)
                {
                    //viewModel = _mapper.Map<AwardsViewModel>(new Award());
                    viewModel.IsActive = true;
                }
                else
                {
                    var award = (await _award.FindAsync(x => x.AwardId == query.AwardId)).FirstOrDefault();
                    
                    viewModel = _mapper.Map<AwardsViewModel>(award);
                }
                var categories = (await _categories.GetAllAsync()).Where(x => x.IsActive == true).OrderBy(x => x.CategoryName).ToList();
                viewModel.Categories = new SelectList(categories.Select(x => new SelectListItem { Text = x.CategoryName, Value = x.CategoryId.ToString() }), "Value", "Text");

                //var eligibilites = (await _eligibilities.GetAllAsync()).Where(x => x.IsActive == true).OrderBy(x => x.Description).ToList();
                //viewModel.Eligibilities = new SelectList(eligibilites.Select(x => new SelectListItem { Text = x.Description, Value = x.EligibilityId.ToString() }), "Value", "Text");
                var mastercodes = (await _mastercodes.GetAllAsync()).ToList();
                var statuses = mastercodes.Where(x => x.CodeType == "Status" && x.IsActive == true);
                viewModel.Statuses = new SelectList(statuses.Select(x => new SelectListItem { Text = x.CodeDesc, Value = x.CodeId.ToString() }), "Value", "Text");
                var months = mastercodes.Where(x => x.CodeType == "Month" && x.IsActive == true);
                viewModel.Months = new SelectList(months.Select(x => new SelectListItem { Text = x.CodeDesc, Value = x.Code }), "Value", "Text");
                return viewModel;
            }
        }

        public class AwardsViewCommand : IRequest<LDAPResult>
        {
            public readonly AwardsViewModel command;

            public AwardsViewCommand(AwardsViewModel command)
            {
                this.command = command;
            }
        }

        public class AwardsViewCommandHandler : IRequestHandler<AwardsViewCommand, LDAPResult>
        {
            private readonly IRepositoryAsync<Award> _award;
            private readonly IRepositoryAsync<Session> _sessions;
            private readonly ILogger<AwardsViewCommandHandler> _logger;
            private readonly IRepositoryAsync<Audit> _audit;
            private readonly IHttpContextAccessor _context;
            readonly IMapper _mapper;
            public AwardsViewCommandHandler(IRepositoryAsync<Award> award,
                IRepositoryAsync<Session> sessions,
                ILogger<AwardsViewCommandHandler> logger,
                IRepositoryAsync<Audit> audit,
                IHttpContextAccessor context,
                IMapper mapper)
            {
                _award = award;
                _logger = logger;
                _sessions = sessions;
                _audit = audit;
                _context = context;
                _mapper = mapper;   
            }

            public async Task<LDAPResult> Handle(AwardsViewCommand message, CancellationToken token)
            {
                var response = new LDAPResult();
                response.Success = true;
                if (message == null)
                {
                    _logger.LogError(new ArgumentNullException(), "The command object was null", null);
                    response.Success = false;
                    response.Errors.Add("Model cannot be null");
                    return response;
                }

                var award = message.command;
                var audit = new Audit();

                if (!string.IsNullOrEmpty(award.AwardUrl) && award.AwardUrl.StartsWith("www"))
                {
                    award.AwardUrl = "https://" + award.AwardUrl;
                }

                if (award.AwardId == 0)
                {
                    var mod = new Award();
                    mod = _mapper.Map<Award>(award);
                    mod.Status = 1;
                    //if (!string.IsNullOrEmpty(award.ApproxDeadline))
                    //{
                    //    mod.DeadlineMonth = Convert.ToInt32(award.ApproxDeadline);
                    //}

                    _ = await _award.AddAsync(mod);

                    var obj = JsonConvert.SerializeObject(mod);
                    audit.Id = new Guid();
                    audit.DateAdded = DateTime.Now;
                    audit.TableName = "Awards";
                    audit.AuditType = "Create";
                    audit.UserName = _context.HttpContext.User.Identity.Name;
                    audit.KeyValues = "AwardId: " + mod.AwardId.ToString();
                    audit.NewValues = obj;

                    var ret = await _audit.AddAsync(audit);
                    //var session = new Session()
                    //{
                    //    AwardId = award.AwardId,
                    //    Deadline = award.Deadline,
                    //    IsActive = award.Deadline > DateTime.Now? true: false
                    //};

                    //_ = await _sessions.AddAsync(session);
                }
                else
                {
                    var mod = await _award.GetAsync(award.AwardId);
                    var oldObj = JsonConvert.SerializeObject(mod);
                    mod = _mapper.Map<Award>(award);
                    var model = _mapper.Map<Award>(award);
                    var newObj = JsonConvert.SerializeObject(model);
                    audit.Id = new Guid();
                    audit.DateAdded = DateTime.Now;
                    audit.TableName = "Awards";
                    audit.AuditType = "Update";
                    audit.UserName = _context.HttpContext.User.Identity.Name;
                    audit.KeyValues = "AwardId: " + mod.AwardId.ToString();
                    audit.NewValues = newObj;
                    audit.OldValues = oldObj;
                    audit.ChangedColumns = new AwardsCheckChanges().GetChangedColumns(model, mod);
                    
                    //mod.Status = 1; 
                    // mod.Deadline == null ? 1 : (mod.Deadline > DateTime.Now ? 1 : 2);
                    //if (!string.IsNullOrEmpty(award.ApproxDeadline))
                    //{
                    //    mod.DeadlineMonth = Convert.ToInt32(award.ApproxDeadline);
                    //}

                    try
                    {
                        var ret = await _award.SaveAsync(mod);
                        ret = await _audit.AddAsync(audit);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message, ex.Message, null);
                        response.Success = false;
                        response.Errors.Add(ex.Message);
                    }
                }

                return response;
            }
        }//end class AwardsViewCommandHandler
    }//end class AwardsViewModel
}//end namespace
