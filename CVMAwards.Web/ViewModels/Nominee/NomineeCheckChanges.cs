﻿using System.Text;

namespace CVMAwards.Web.ViewModels.Nominee
{
    public class NomineeCheckChanges
    {
        public string GetChangedColumns(Core.Entities.Nominee Old, Core.Entities.Nominee New)
        {
            var changes = new StringBuilder();

            if (!Old.Name.Equals(New.Name))
            {
                changes.Append("Name; ");
            }

            if (!Old.NominatedBy.Equals(New.NominatedBy))
            {
                changes.Append("Nominated By; ");
            }

            if (Old.NominatedOn != New.NominatedOn)
            {
                changes.Append("Nominated On; ");
            }
            return changes.ToString();
        }
    }
}
