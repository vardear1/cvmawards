﻿using AutoMapper;
using CVMAwards.Core.Entities;
using CVMAwards.Infrastructure.Repository;
using CVMAwards.Web.Models.LDAP;
using MediatR;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Web.ViewModels.Nominee
{
    public class NomineeViewModel
    {
        [Key]
        public int NomineeId { get; set; }
        public int AwardId { get; set; }
        [Display(Name = "Nominee Name")]
        [Required(ErrorMessage = "Nominee Name is required")]
        public string? Name { get; set; }
        public string? NominatedBy { get; set; }
        public DateTime NominatedOn { get; set; }
        public string? AwardName { get; set; }
        public class NomineeQuery : IRequest<NomineeViewModel>
        {
            public int AwardId { get; set; }
            public int NomineeId { get; set; }
        }

        public class NomineeQueryResult : IRequestHandler<NomineeQuery, NomineeViewModel>
        {
            private IRepositoryAsync<Core.Entities.Nominee> _nominees;
            private IRepositoryAsync<Award> _awards;
            readonly IMapper _mapper;
            public NomineeQueryResult(IRepositoryAsync<Core.Entities.Nominee> nominees,
                IRepositoryAsync<Award> awards,
                IMapper mapper
                )
            {
                _nominees = nominees;
                _awards = awards;
                _mapper = mapper;
            }
            public async Task<NomineeViewModel> Handle(NomineeQuery query, CancellationToken token)
            {
                var result = new NomineeViewModel();

                var award = (await _awards.GetAllAsync()).Where(x => x.AwardId == query.AwardId).First();
                var nominee = (await _nominees.GetAllAsync()).Where(x => x.AwardId == query.AwardId).FirstOrDefault();

                if (nominee != null)
                    result = _mapper.Map<NomineeViewModel>(nominee);

                result.AwardId = query.AwardId;
                result.AwardName = award.AwardName;
                return result;
            }
        }

        public class NomineeCommand : IRequest<LDAPResult>
        {
            public readonly NomineeViewModel command;

            public NomineeCommand(NomineeViewModel command)
            {
                this.command = command;
            }
        }

        public class NomineeCommandHandler : IRequestHandler<NomineeCommand, LDAPResult>
        {
            private IRepositoryAsync<Core.Entities.Nominee> _nominees;
            private ILogger<NomineeCommandHandler> _logger;
            private readonly IRepositoryAsync<Audit> _audit;
            private readonly IHttpContextAccessor _context;
            readonly IMapper _mapper;
            public NomineeCommandHandler(IRepositoryAsync<Core.Entities.Nominee> nominees,
                ILogger<NomineeCommandHandler> logger,
                IRepositoryAsync<Audit> audit,
                IHttpContextAccessor context,
                IMapper mapper)
            {
                _nominees = nominees;
                _logger = logger;
                _audit = audit;
                _context = context;
                _mapper = mapper;
            }

            public async Task<LDAPResult> Handle(NomineeCommand message, CancellationToken token)
            {
                if (message == null)
                {
                    _logger.LogError(new ArgumentNullException(), "The command object was null", null);
                    throw new ArgumentNullException("Command");
                }

                var result = new LDAPResult();
                result.Success = true;

                var nominee = message.command;
                var audit = new Audit();
                audit.Id = new Guid();
                audit.DateAdded = DateTime.Now;
                audit.TableName = "Nominees";
                audit.UserName = _context.HttpContext.User.Identity.Name;


                var model1 = (await _nominees.GetAllAsync()).Where(x => x.AwardId == nominee.AwardId).FirstOrDefault();
                if (model1 != null)
                {
                    throw new Exception("Nominated");
                }

                if (nominee.NomineeId > 0)
                {
                    var mod = (await _nominees.GetAllAsync()).Where(x => x.NomineeId == nominee.NomineeId).FirstOrDefault();
                    var oldnominee = JsonConvert.SerializeObject(mod);
                    var model = _mapper.Map<Core.Entities.Nominee>(nominee);
                    model.NominatedBy = _context.HttpContext.User.Identity.Name;
                    model.NominatedOn = DateTime.Now;
                    var newnominee = JsonConvert.SerializeObject(model);

                    mod = _mapper.Map<Core.Entities.Nominee>(nominee);
                    mod.NominatedBy = _context.HttpContext.User.Identity.Name;
                    mod.NominatedOn = DateTime.Now;

                    var ret = await _nominees.SaveAsync(mod);

                    audit.AuditType = "Update";
                    audit.KeyValues = "NomineeId: " + mod.NomineeId.ToString();
                    audit.NewValues = newnominee;
                    audit.OldValues = oldnominee;
                    audit.ChangedColumns = new NomineeCheckChanges().GetChangedColumns(model, mod);
                    ret = await _audit.AddAsync(audit);
                }
                else
                {
                    try
                    {
                        var mod = new Core.Entities.Nominee();
                        mod = _mapper.Map< Core.Entities.Nominee>(nominee);
                        mod.NominatedBy = _context.HttpContext.User.Identity.Name;
                        mod.NominatedOn = DateTime.Now;

                        _ = await _nominees.AddAsync(mod);

                        var obj = JsonConvert.SerializeObject(mod);

                        audit.AuditType = "Create";
                        audit.KeyValues = "NomineeId: " + mod.NomineeId.ToString();
                        audit.NewValues = obj;

                        var ret = await _audit.AddAsync(audit);
                    }
                    catch (Exception ex)
                    {
                        string mess = ex.Message;
                    }
                }
                return result;
            }
        }
    }//end class NomineeViewModel
}//end namespace