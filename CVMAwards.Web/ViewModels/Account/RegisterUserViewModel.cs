﻿using AutoMapper;
using CVMAwards.Core.Account;
using CVMAwards.Web.Models.LDAP;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace CVMAwards.Web.ViewModels.Account
{
    public class RegisterUserViewModel
    {
        public class RegisterUserCommand : IRequest<LDAPResult>
        {
            public readonly RegisterViewModel Model;
            public RegisterUserCommand(RegisterViewModel Model)
            {
                this.Model = Model;
            }
        }

        public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, LDAPResult>
        {
            private readonly UserManager<ApplicationUser> userManager;
            readonly IMapper _mapper;
            public RegisterUserCommandHandler(UserManager<ApplicationUser> userManager,
                IMapper mapper)
            {
                this.userManager = userManager;
                _mapper = mapper;
            }

            public async Task<LDAPResult> Handle(RegisterUserCommand message, CancellationToken cancellationToken)
            {
                var result = new LDAPResult();
                result.Success = true;
                var model = message.Model;
                ApplicationUser user = new ApplicationUser();
                user = _mapper.Map(model, user);
                user.IsActive = true;
                user.IsDomainUser = false;
                var response = await userManager.CreateAsync(user, model.Password);

                if (!response.Succeeded)
                {
                    result.Success = false;
                    foreach(var error in response.Errors)
                    {
                        result.Errors.Add(error.Description);
                    }
                }

                return result;
            }
        }
    }//end class RegisterUserViewModel
}//end namespace
