﻿using CVMAwards.Web.Models.LDAP;
using CVMAwards.Web.ViewModels.Account;

namespace CVMAwards.Web.Services
{
    public interface IAuthAPIService
    {
        Task<bool> Login(LoginViewModel model);
        //Task<IEnumerable<string>> GetValues();
        Task<LdapUserImportViewModel> GetLDAPUsers(LDAPQueryModel query);
    }
}
