﻿using CVMAwards.Web.Models.LDAP;
using CVMAwards.Web.ViewModels.Account;
using Newtonsoft.Json;
using System.Text;

namespace CVMAwards.Web.Services
{
    public class AuthAPIService : IAuthAPIService
    {
        public HttpClient Client { get; }
        readonly ILogger<AuthAPIService> logger;
        public AuthAPIService(HttpClient client,
            ILogger<AuthAPIService> logger)
        {
            Client = client;
            this.logger = logger;
        }

        public async Task<LdapUserImportViewModel> GetLDAPUsers(LDAPQueryModel query)
        {
            var response = (await Client.GetAsync($"cvmauth/api/Auth/Getusers?showExistingUsers={query.ShowExistingUsers}&userFilter={query.UserFilter}&filteredGroup={query.FilteredGroup}"));
            var result = response.Content.ReadAsStringAsync().Result;
            var model = JsonConvert.DeserializeObject<LdapUserImportViewModel>(result);

            return model;
        }

        public async Task<bool> Login(LoginViewModel model)
        {
            try
            {
                HttpContent body = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var response = (await Client.PostAsync("cvmauth/api/auth/Login", body));
                var result = response.Content.ReadAsStringAsync().Result;
                var obj = JsonConvert.DeserializeObject<LoginViewModel>(result);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "error in login", null);
                return false;
            }
        }
    }
}
