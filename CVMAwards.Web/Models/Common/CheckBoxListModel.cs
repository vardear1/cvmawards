﻿using CVMAwards.Core.Entities;

namespace CVMAwards.Web.Models.Common
{
    public class CheckBoxListModel
    {
        public int Id { get; set; }
        public string? StringId { get; set; }
        public string? Name { get; set; }
        public bool Checked { get; set; }

        public List<CheckBoxListModel> SetCheckBoxList(List<MasterCode> list)
        {
            List<CheckBoxListModel> mod = new List<CheckBoxListModel>();
            CheckBoxListModel model;

            foreach (var item in list)
            {
                model = new CheckBoxListModel();
                model.Id = item.CodeId;
                model.Name = item.CodeDesc;
                model.Checked = false;

                mod.Add(model);
            }
            return mod;
        }
    }
}

