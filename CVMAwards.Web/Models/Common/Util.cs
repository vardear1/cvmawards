﻿namespace CVMAwards.Web.Models.Common
{
    public class Util
    {
        private static IConfiguration configuration;
        private static IConfigurationSection? section;
        private static string? baseUrl;
        static Util()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            configuration = builder.Build();
        }

        public static string GetBaseUrl
        {
            get
            {
                //return string.IsNullOrEmpty(baseUrl) ? "" : baseUrl;
                var section = configuration.GetSection("AppSettings");
                var env = section.GetValue<string>("env");

                if (env.ToUpper() == "DEV")
                {
                    return section.GetValue<string>("BaseURLDev");
                }
                else if (env.ToUpper() == "STAGING")
                {
                    return section.GetValue<string>("BaseURLTest");
                }
                else
                {
                    return section.GetValue<string>("BaseURLProd");
                }
                //section = configuration.GetSection("AppSettings");

            }
        }

        public static string CashnetUrl
        {
            get
            {
                section = configuration.GetSection("AppSettings");
                var env = section.GetValue<string>("env");
                if (env.ToUpper() == "DEV")
                {
                    return section.GetValue<string>("cashnettest");
                }
                else if (env.ToUpper() == "STAGING")
                {
                    return section.GetValue<string>("cashnettest");
                }
                else
                {
                    return section.GetValue<string>("cashnetprod");
                }
            }

        }

        public static string GetToEmailAddress
        {
            get
            {
                section = configuration.GetSection("EmailSettings");
                return section.GetValue<string>("ToEmail");
            }
        }

        public static string GetToEmailName
        {
            get
            {
                section = configuration.GetSection("EmailSettings");
                return section.GetValue<string>("ToName");
            }
        }

        public static string GetFromEmailName
        {
            get
            {
                section = configuration.GetSection("EmailSettings");
                return section.GetValue<string>("SMTPFromName");
            }
        }

        public static string GetFromEmail
        {
            get
            {
                section = configuration.GetSection("EmailSettings");
                return section.GetValue<string>("SMTPFrom");
            }
        }

        public static string EmailHost
        {
            get
            {
                section = configuration.GetSection("EmailSettings");
                return section.GetValue<string>("Host");
            }
        }

        public static int EmailPort
        {
            get
            {
                section = configuration.GetSection("EmailSettings");
                return Convert.ToInt32(section.GetValue<string>("Port"));
            }
        }

        public static string HelpDeskEmail
        {
            get
            {
                section = configuration.GetSection("EmailSettings");
                return section.GetValue<string>("HelpDeskEmail");
            }
        }
    }
}
