﻿namespace CVMAwards.Web.Models.Common
{
    public class BootstrapModal
    {
        public string? ID { get; set; }
        public string? AreaLabeledId { get; set; }
    }
}
