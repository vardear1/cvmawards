﻿using System.Net.Mail;

namespace CVMAwards.Web.Models.Common
{
    public class Email
    {
        string mailToAddress;
        string mailCCAddress;
        string mailBCCAddress;
        string mailFromAddress;
        string mailFromName;
        string mailBody;
        string mailSubject;
        public Email()
        {

        }

        public string FromAddress
        {
            get
            {
                if (!(mailFromAddress == null))
                    return mailFromAddress;
                else
                    return Util.GetFromEmail;
            }
            set { mailFromAddress = value; }
        }

        public string FromName
        {
            get
            {
                if (!(mailFromName == null))
                    return mailFromName;
                else
                    return Util.GetFromEmailName;
            }
            set { mailFromName = value; }
        }

        public string HelpDeskEmail
        {
            get { return Util.HelpDeskEmail; }
        }
        public string ToAddress
        {
            get
            {
                if (!(mailToAddress == null))
                    return mailToAddress;
                else
                    return Util.GetToEmailAddress;
            }
            set { mailToAddress = value; }
        }

        public string CCAddress
        {
            get { return mailCCAddress; }
            set { mailCCAddress = value; }
        }

        public string Body
        {
            get { return mailBody; }
            set { mailBody = value; }
        }

        public string Subject
        {
            get { return mailSubject; }
            set { mailSubject = value; }
        }

        public int SMTPPort
        {
            get { return Util.EmailPort; }
        }

        public string SMTPHost
        {
            get { return Util.EmailHost; }
        }
        public void SendEmail()
        {
            MailMessage mm = new MailMessage();

            mm.From = new MailAddress(FromAddress, FromName);

            if (string.IsNullOrEmpty(ToAddress))
                throw new Exception("User email address missing.");

            foreach (var emailTo in ToAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                mm.To.Add(emailTo);
            }

            if (!string.IsNullOrEmpty(CCAddress))
            {
                foreach (var emailcc in CCAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mm.CC.Add(emailcc);
                }
            }

            mm.Subject = Subject;
            mm.Body = Body;
            mm.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = true;
            smtp.Host = SMTPHost;
            smtp.Port = SMTPPort;

            smtp.Send(mm);
        }//end method sendemail
    }//end class email
}//end namespace


//SmtpClient client = new SmtpClient("mysmtpserver");
//client.UseDefaultCredentials = false;
//client.Credentials = new NetworkCredential("username", "password");

//MailMessage mailMessage = new MailMessage();
//mailMessage.From = new MailAddress("whoever@me.com");
//mailMessage.To.Add("receiver@me.com");
//mailMessage.Body = "body";
//mailMessage.Subject = "subject";
//client.Send(mailMessage);