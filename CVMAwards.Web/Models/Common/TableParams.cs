﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace CVMAwards.Web.Models.Common
{
    public class TableParams
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public int ColIndex { get; set; }
        public string? SortDir { get; set; }
        public string? Search { get; set; }
        public string? UserName { get; set; }
    }
    public class DataTableParams
    {
        public int Draw { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public ColumnRequestItem[] Columns { get; set; }
        public OrderRequestItem[] Order { get; set; }
        public SearchRequestItem Search { get; set; }
    }

    public class ColumnRequestItem
    {
        public string? Data { get; set; }
        public string? Name { get; set; }
        public bool Searchable { get; set; }
        public bool Orderable { get; set; }
        public SearchRequestItem? Search { get; set; }
    }

    public class OrderRequestItem
    {
        public int Column { get; set; }
        public string? Dir { get; set; }
    }

    public class SearchRequestItem
    {
        public string? Value { get; set; }
        public bool Regex { get; set; }
    }

    public class Search
    {
        public int OrderId { get; set; }
        public string? FromDate { get; set; }
        public string? ToDate { get; set; }
        public string? VendorName { get; set; }
        public string? UserName { get; set; }
        public string? ItemDesc { get; set; }
        public string? PartNum { get; set; }
        public string? Status { get; set; }

        public SelectList? StatusList { get; set; }
        public SelectList? Users { get; set; }
    }

    public class ScheduleListParms : DataTableParams
    {
        public int Id { get; set; }
        public string? Username { get; set; }
    }

}
