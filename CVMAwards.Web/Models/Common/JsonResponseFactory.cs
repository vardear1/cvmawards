﻿namespace CVMAwards.Web.Models.Common
{
    public class JsonResponseFactory
    {
        public static object ErrorResponse(string error)
        {
            return new { success = false, message = error };
        }

        public static object SuccessResponse()
        {
            return new { success = true };
        }

        public static object SuccessResponse(string message)
        {
            return new { success = true, message = message };
        }
        public static object SuccessResponse(object referenceObject)
        {
            return new { success = true, Object = referenceObject };
        }
    }
}

