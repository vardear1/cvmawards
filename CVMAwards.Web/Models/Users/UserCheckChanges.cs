﻿using CVMAwards.Core.Account;
using System.Text;

namespace CVMAwards.Web.Models.Users
{
    public class UserCheckChanges
    {
        public static string GetChangedColumns(ApplicationUser Old, ApplicationUser New)
        {
            var changes = new StringBuilder();

            if (!string.Equals(Old.FirstName, New.FirstName))
            {
                changes.Append("First Name; ");
            }
            if (!string.Equals(Old.LastName, New.LastName))
            {
                changes.Append("Last Name; ");
            }
            if (!string.Equals(Old.Email, New.Email))
            {
                changes.Append("Email; ");
            }
            if (Old.IsActive != New.IsActive)
            {
                changes.Append("Is Active; ");
            }
            if (Old.IsDomainUser != New.IsDomainUser)
            {
                changes.Append("Is Domain User; ");
            }
            return changes.ToString();
        }
    }
}
