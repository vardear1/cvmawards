﻿using AutoMapper;
using CVMAwards.Core.Account;
using CVMAwards.Core.Entities;
using CVMAwards.Infrastructure.DAL;
using CVMAwards.Infrastructure.Repository;
using CVMAwards.Web.Models.Common;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace CVMAwards.Web.Models.Users
{
    public class AspNetUserViewModel
    {
        public ApplicationUser User { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "Password Confirm")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password confirm is required")]
        [Compare("Password", ErrorMessage = "Confirm password does not match password")]
        public string? PasswordConfirm { get; set; }
        public List<ApplicationUser> Users { get; set; }
        [Display(Name = "User Role")]
        [Required(ErrorMessage = "Role is required")]
        public string? UserRole { get; set; }
        public SelectList Roles { get; set; }
        public List<AspNetUserRole> UserRoles { get; set; }
        [Display(Name = "User Role")]
        [Required(ErrorMessage = "Role is required")]
        public List<CheckBoxListModel> RolesList { get; set; }
        public string? Type { get; set; }

        public class AspNetUserViewModelQuery : IRequest<AspNetUserViewModel> //IAsyncRequest<AspNetUserViewModel>
        {
            public string? UserId { get; set; }
            public string? UserName { get; set; }
            public string? Type { get; set; }
        }

        public class AspNetUserViewModelRequestHandler : IRequestHandler<AspNetUserViewModelQuery, AspNetUserViewModel> //IAsyncResult<AspNetUserViewModelQuery, AspNetUserViewModel> // IAsyncRequestHandler<AspNetUserViewModelQuery, AspNetUserViewModel>
        {

            private readonly ILogger<AspNetUserViewModel> _logger;
            private readonly RoleManager<ApplicationRole> _roleManager;
            private readonly UserManager<ApplicationUser> _user;
            public AspNetUserViewModelRequestHandler(
                ILogger<AspNetUserViewModel> logger,
                RoleManager<ApplicationRole> roleManager,
                UserManager<ApplicationUser> user
                )
            {

                _logger = logger;
                _roleManager = roleManager;
                _user = user;
            }

            public async Task<AspNetUserViewModel> Handle(AspNetUserViewModelQuery query, CancellationToken cancellationToken)
            {
                AspNetUserViewModel result = new AspNetUserViewModel();

                if (string.IsNullOrEmpty(query.Type))
                {

                    result.Users = await _user.Users.ToListAsync(); //_db.AspNetUsers.ToListAsync();                    
                }
                else
                {
                    ApplicationUser user;
                    result.Type = "Edit User";
                    if (query.Type == "Add")
                    {
                        user = new ApplicationUser();
                        result.Type = "Add New User";
                    }
                    else if (!(string.IsNullOrEmpty(query.UserId)))
                    {
                        user = await _user.FindByIdAsync(query.UserId); //users.Where(x => x.Id == query.UserId).FirstOrDefault();
                    }
                    else
                    {
                        user = await _user.FindByNameAsync(query.UserName); //users.Where(x => x.UserName == query.UserName).FirstOrDefault();
                    }
                    result.User = user;
                    result.UserRoles = await new AspNetUserViewModel().GetUserRoles(user.Id, _user, _roleManager); // await _db.AspNetUserRoles.Where(x => x.UserId == user.Id).ToListAsync();

                    new AspNetUserViewModel().SetUserRoles(result, _roleManager);
                }
                return result;
            }
        }//end class AspNetUserViewModelRequestHandler

        public class AspNetUserViewModelCommand : IRequest<bool> //ICancellableAsyncRequest<Unit>
        {
            public readonly AspNetUserViewModel command;

            public AspNetUserViewModelCommand(AspNetUserViewModel command)
            {
                this.command = command;
            }
        }

        public class AspNetUserViewModelCommandHandler : IRequestHandler<AspNetUserViewModelCommand, bool> //ICancellableAsyncRequestHandler<AspNetUserViewModelCommand, Unit>
        {
            private readonly IRepositoryAsync<ApplicationUser> _users;
            private readonly AwardsDbContext _db;
            private readonly ILogger<AspNetUserViewModel> _logger;
            private readonly RoleManager<ApplicationRole> _roleManager;
            private readonly UserManager<ApplicationUser> _user;
            private readonly IRepositoryAsync<Audit> _audit;
            readonly IMapper _mapper;
            readonly IHttpContextAccessor _contextAccessor;
            public AspNetUserViewModelCommandHandler(IRepositoryAsync<ApplicationUser> users,
                AwardsDbContext db,
                ILogger<AspNetUserViewModel> logger,
                RoleManager<ApplicationRole> roleManager,
                UserManager<ApplicationUser> user,
                IRepositoryAsync<Audit> audit,
                IMapper mapper,
                IHttpContextAccessor contextAccessor)
            {
                _users = users;
                _db = db;
                _logger = logger;
                _roleManager = roleManager;
                _user = user;
                _audit = audit;
                _mapper = mapper;
                _contextAccessor = contextAccessor;
            }

            public async Task<bool> Handle(AspNetUserViewModelCommand message, CancellationToken token)
            {
                if (message == null)
                {
                    throw new ArgumentNullException("Command");
                }

                ApplicationUser user = message.command.User;
                var UserRoles = message.command.RolesList;

                var audit = new Audit();
                audit.Id = new Guid();
                audit.DateAdded = DateTime.Now;
                audit.TableName = "AspNetUsers";
                audit.UserName = _contextAccessor.HttpContext.User.Identity.Name;

                try
                {
                    if (string.IsNullOrEmpty(user.Id))
                    {
                        await _user.CreateAsync(user, message.command.Password); //_db.AddAsync(user);
                        await new AspNetUserViewModel().SaveRoles(user, UserRoles, _user, _roleManager, _audit);

                        audit.KeyValues = "User Id: " + user.Id.ToString();
                        audit.AuditType = "Create";
                        audit.NewValues = JsonConvert.SerializeObject(user);
                        var count = await _audit.AddAsync(audit);
                    }
                    else
                    {
                        var existingUser = await _user.FindByIdAsync(user.Id);

                        if (existingUser == null)
                        {
                            await _user.CreateAsync(user, message.command.Password);
                        }
                        else
                        {
                            audit.OldValues = JsonConvert.SerializeObject(existingUser);
                            audit.NewValues = JsonConvert.SerializeObject(user);
                            audit.AuditType = "Update";
                            audit.KeyValues = "User Id: " + user.Id.ToString();
                            audit.ChangedColumns = UserCheckChanges.GetChangedColumns(existingUser, user);

                            existingUser = await _user.FindByNameAsync(user.UserName);
                            existingUser.FirstName = user.FirstName;
                            existingUser.LastName = user.LastName;
                            existingUser.Email = user.Email;
                            existingUser.IsActive = user.IsActive;
                            existingUser.IsDomainUser = user.IsDomainUser;

                            await _user.UpdateAsync(existingUser);
                            var count = await _audit.AddAsync(audit);
                        }

                        await new AspNetUserViewModel().SaveRoles(existingUser, UserRoles, _user, _roleManager, _audit);
                        //_db.Entry(user).State = EntityState.Modified;
                    }

                    
                    //await _db.SaveChangesAsync(token);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    return false;
                }

                return true;
                //return Unit.Value;
            }
        }//end class AspNetUserViewModelCommandHandler

        private AspNetUserViewModel SetUserRoles(AspNetUserViewModel model, RoleManager<ApplicationRole> roleManager)
        {
            CheckBoxListModel mod;
            var roles = roleManager.Roles;

            if (model.RolesList != null)
            {
                foreach (var role in roles)
                {
                    foreach (var item in model.RolesList)
                    {
                        if (role.Id == item.StringId)
                        {
                            item.Name = role.Name;
                            break;
                        }
                    }
                }
            }
            else
            {
                model.RolesList = new List<CheckBoxListModel>();

                foreach (var item in roles)
                {
                    mod = new CheckBoxListModel();
                    mod.StringId = item.Id;
                    mod.Name = item.Name;
                    mod.Checked = false;
                    model.RolesList.Add(mod);
                }

                if (model.UserRoles != null)
                {
                    foreach (var role in model.UserRoles)
                    {
                        foreach (var item in model.RolesList)
                        {
                            if (role.RoleId == item.StringId)
                            {
                                item.Checked = true;
                                break;
                            }
                        }
                    }
                }
            }

            return model;
        }

        private async Task<List<AspNetUserRole>> GetUserRoles(string userid, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            List<AspNetUserRole> list = new List<AspNetUserRole>();
            AspNetUserRole role;

            var user = await userManager.FindByIdAsync(userid);

            if (user != null)
            {
                var useroles = await userManager.GetRolesAsync(user);

                foreach (var userole in useroles)
                {
                    role = new AspNetUserRole();
                    role.UserId = userid;
                    role.RoleId = (await roleManager.FindByNameAsync(userole)).Id;
                    list.Add(role);
                }
            }


            //DBManage db = new DBManage();
            //List<SqlParameter> input = new List<SqlParameter>();

            //input.Add(DBManage.setInputParameter<string>("UserId", userid, 50));
            //DataTable dt = db.GetDataTable("GetUserRoles", input.ToArray());

            //foreach (DataRow dr in dt.Rows)
            //{
            //    role = new AspNetUserRole();
            //    role.UserId = userid;
            //    role.RoleId = dr["RoleId"].ToString();
            //    list.Add(role);
            //}
            return list;

        }

        private async Task SaveRoles(ApplicationUser user, List<CheckBoxListModel> userRoles, 
            UserManager<ApplicationUser> userManager, 
            RoleManager<ApplicationRole> roleManager,
            IRepositoryAsync<Audit> _audit)
        {
            var roles = await roleManager.Roles.ToListAsync();

            var audit = new Audit();
            audit.Id = new Guid();
            audit.DateAdded = DateTime.Now;
            audit.TableName = "AspNetUserRoles";
            audit.UserName = user.UserName;

            if (roles.Count > 0)
            {
                audit.AuditType = "Update";
                audit.OldValues = "";
            }
            else
            {
                audit.AuditType = "Create";
                audit.NewValues = string.Empty;
            }
            foreach (var item in roles)
            {
                audit.OldValues = audit.OldValues + item.Name + "; ";
                await userManager.RemoveFromRoleAsync(user, item.Name);
            }

            foreach (var item in userRoles)
            {
                if (item.Checked)
                {
                    var role = await roleManager.FindByIdAsync(item.StringId);
                    await userManager.AddToRoleAsync(user, role.Name);
                    audit.KeyValues = "Role Id: " + item.StringId + ";";
                    audit.NewValues = audit.NewValues + role.Name + ";";
                }
            }
            var count = await _audit.AddAsync(audit);
        }
        public async Task<List<string>> GetErrors(
                AspNetUserViewModel model,
                //CVMFormsDBcontext _db, 
                RoleManager<ApplicationRole> roleManager,
                UserManager<ApplicationUser> userManager
            )
        {
            List<string> list = new List<string>();

            var user = model.User;

            if (string.IsNullOrEmpty(user.Id))
            {
                var aspUser = await userManager.FindByNameAsync(user.UserName); //_db.AspNetUsers.Where(x => x.UserName == user.UserName).SingleOrDefault();
                if (aspUser != null)
                {
                    list.Add("User name already exists. Please enter a unique user name.");
                }

                aspUser = await userManager.FindByEmailAsync(user.Email); // _db.AspNetUsers.Where(x => x.Email == user.Email).SingleOrDefault();

                if (aspUser != null)
                {
                    list.Add("Email already exists. Please enter a unique email");
                }
            }

            if (!(string.IsNullOrEmpty(user.Id)))
            {
                var aspUser = await userManager.FindByIdAsync(user.Id); //_db.AspNetUsers.Where(x => x.Id == user.Id).SingleOrDefault();

                if (aspUser == null)
                {
                    var existinguser = await userManager.FindByNameAsync(user.UserName);
                    if (existinguser != null)
                    {
                        list.Add("User name already exists. Please enter a unique user name.");
                    }

                    existinguser = await userManager.FindByEmailAsync(user.Email);
                    if (existinguser != null)
                    {
                        list.Add("Email already exists. Please enter a unique email");
                    }

                }
                else if (aspUser.Email != user.Email)
                {
                    var mod = await userManager.FindByEmailAsync(user.Email); //_db.AspNetUsers.Where(x => x.Email == user.Email).SingleOrDefault();

                    if (mod != null)
                    {
                        list.Add("Email already exists. Please enter a unique email.");
                    }
                }
            }

            var roleAdded = false;

            foreach (var item in model.RolesList)
            {
                if (item.Checked)
                {
                    roleAdded = true;
                    break;
                }
            }

            if (!roleAdded)
            {
                list.Add("At least one role must be selected");
            }

            if (list.Count() > 0)
            {
                SetUserRoles(model, roleManager);
            }
            return list;
        }
    }
}
