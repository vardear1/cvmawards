﻿namespace CVMAwards.Web.Models.LDAP
{
    public class LDAPGroup
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? DistinguishedName { get; set; }
    }
}
