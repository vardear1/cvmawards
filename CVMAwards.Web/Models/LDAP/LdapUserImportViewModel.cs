﻿using CVMAwards.Core.Account;
using CVMAwards.Core.Entities;
using CVMAwards.Infrastructure.DAL;
using CVMAwards.Infrastructure.Repository;
using CVMAwards.Web.Models.Users;
using CVMAwards.Web.Services;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace CVMAwards.Web.Models.LDAP
{
    public class LdapUserImportViewModel
    {
        public List<LDAPUser> Users { get; set; }
        public List<SelectListItem> Groups { get; set; }
        public bool ShowExistingUsers { get; set; }
        public string? UserFilter { get; set; }
        public string? FilteredGroup { get; set; }
        public IEnumerable<SelectListItem> GroupsList { get; set; }

        public class LdapUserImportQuery : IRequest<LdapUserImportViewModel>
        {
            public bool ShowExistingUsers { get; set; }
            public string? UserFilter { get; set; }
            public string? FilteredGroup { get; set; }
        }

        public class LdapUserImportRequestHandler : IRequestHandler<LdapUserImportQuery, LdapUserImportViewModel>
        {
            readonly UserManager<ApplicationUser> _userManager;
            readonly IAuthAPIService _service;
            public LdapUserImportRequestHandler(
                UserManager<ApplicationUser> userManager,
                IAuthAPIService service)
            {
                _userManager = userManager;
                _service = service;
            }

            public async Task<LdapUserImportViewModel> Handle(LdapUserImportQuery message, CancellationToken cancellationToken)
            {
                if (message.FilteredGroup == "AllUsers")
                {
                    message.FilteredGroup = null;
                }

                var ldapUsers = new List<LDAPUser>();
                var localUserNames = new List<string>();
                localUserNames = await _userManager.Users.Select(x => x.UserName).ToListAsync();

                var query = new LDAPQueryModel()
                {
                    ShowExistingUsers = message.ShowExistingUsers,
                    UserFilter = message.UserFilter,
                    FilteredGroup = message.FilteredGroup
                };

                var response = await _service.GetLDAPUsers(query);
                ldapUsers = response.Users;

                var userList = new List<LDAPUser>();

                foreach (var ldapUser in ldapUsers)
                {
                    var user = ldapUser;
                    foreach (var username in localUserNames)
                    {
                        if (user.UserName == username)
                        {
                            ldapUser.Imported = true;
                        }
                    }

                    if (message.ShowExistingUsers && ldapUser.Imported && !userList.Contains(ldapUser))
                    {
                        userList.Add(ldapUser);
                    }
                    else if (ldapUser.Imported == false && !userList.Contains(ldapUser))
                    {
                        userList.Add(ldapUser);
                    }
                }

                var model = new LdapUserImportViewModel
                {
                    ShowExistingUsers = message.ShowExistingUsers,
                    Users = userList.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToList(),
                    UserFilter = message.UserFilter,
                    Groups = response.Groups
                };
                model.Groups = model.Groups.OrderBy(x => x.Text).ToList();

                model.Groups.Insert(0, new SelectListItem { Text = "All Users", Value = "AllUsers" });
                model.Groups.Insert(0, new SelectListItem { Text = "-Select a Group-", Value = "SelectAGroup" });

                int userCount = 1;
                foreach (var user in model.Users)
                {
                    user.Index = userCount++;
                }

                return model;
            }//end method handle
        }//end class class LdapUserImportRequestHandler

        public class LdapUserImportViewModelCommand : IRequest<LDAPResult>
        {
            public readonly AspNetUserViewModel command;

            public LdapUserImportViewModelCommand(AspNetUserViewModel command)
            {
                this.command = command;
            }
        }//end class LdapUserImportViewModelCommand 

        public class LdapUserImportViewModelCommandHandler : IRequestHandler<LdapUserImportViewModelCommand, LDAPResult>
        {
            private readonly UserManager<ApplicationUser> userManager;
            private readonly IRepositoryAsync<AspNetUserRole> _userroles;
            private readonly AwardsDbContext _db;
            private readonly ILogger<LdapUserImportViewModel> _logger;
            private readonly RoleManager<ApplicationRole> _roleManager;
            private readonly IHttpContextAccessor _context;
            public LdapUserImportViewModelCommandHandler(UserManager<ApplicationUser> userManager,
                AwardsDbContext db,
                IRepositoryAsync<AspNetUserRole> userroles,
                ILogger<LdapUserImportViewModel> logger,
                RoleManager<ApplicationRole> roleManager,
                IHttpContextAccessor context)
            {
                this.userManager = userManager;
                _db = db;
                _userroles = userroles;
                _logger = logger;
                _roleManager = roleManager;
                _context = context;
            }

            public async Task<LDAPResult> Handle(LdapUserImportViewModelCommand message, CancellationToken cancellationToken)
            {
                LDAPResult result = new LDAPResult();
                result.Success = true;

                if (message == null || message.command == null)
                {
                    result.Success = false;
                    result.Errors.Add("Model cannot be null");
                    return result;
                }

                var roleAdded = false;

                foreach (var role in message.command.RolesList)
                {
                    if (role.Checked)
                    {
                        roleAdded = true;
                        break;
                    }
                }

                if (!roleAdded)
                {
                    result.Success = false;
                    result.Errors.Add("Role must be selected");
                    return result;
                }

                int importedCount = 0;
                var existingUsers = await userManager.Users.ToListAsync();
                var existingUserNames = await userManager.Users.Select(x => x.UserName).ToListAsync();

                ApplicationUser user = message.command.User;

                if (existingUserNames.Contains(user.UserName))
                {
                    var existingUser = existingUsers.Single(x => x.UserName == user.UserName);

                    existingUser.Email = user.Email;
                    existingUser.UserName = user.UserName;
                    existingUser.FirstName = user.FirstName;
                    existingUser.LastName = user.LastName;
                    existingUser.PhoneNumber = user.PhoneNumber;
                    existingUser.IsActive = user.IsActive;
                    existingUser.IsDomainUser = user.IsDomainUser;
                    existingUser.EmailConfirmed = true;
                    try
                    {
                        await userManager.UpdateAsync(existingUser);

                        await SaveRoles(existingUser, message.command.RolesList, userManager, _roleManager);

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                        result.Success = false;
                        result.Errors.Add("Error saving the user");
                        return result;
                    }
                }
                else
                {
                    try
                    {
                        user.EmailConfirmed = true;
                        var response = await userManager.CreateAsync(user, "0v3r!yK0mp!1c@ted");

                        if (!response.Succeeded)
                        {
                            foreach (var error in response.Errors)
                            {
                                result.Errors.Add(error.Description);
                            }
                            result.Success = false;
                            return result;
                        }

                        await SaveRoles(user, message.command.RolesList, userManager, _roleManager);

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                        result.Success = false;
                        result.Errors.Add("Error saving the user");
                        return result;
                    }
                }

                return result;
            }//end method handle

            private async Task SaveRoles(ApplicationUser user,
                    List<Common.CheckBoxListModel> userRoles,
                    UserManager<ApplicationUser> userManager,
                    RoleManager<ApplicationRole> roleManager)
            {
                var roles = await roleManager.Roles.ToListAsync();

                var rolesList = (await userManager.GetRolesAsync(user)).ToList();

                foreach (var item in roles)
                {
                    await userManager.RemoveFromRoleAsync(user, item.Name);
                }

                foreach (var item in userRoles)
                {
                    if (item.Checked)
                    {
                        var role = await roleManager.FindByIdAsync(item.StringId);
                        await userManager.AddToRoleAsync(user, role.Name);
                    }
                }
            }
        }
    }
}
