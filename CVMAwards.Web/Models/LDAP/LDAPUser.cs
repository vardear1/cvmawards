﻿namespace CVMAwards.Web.Models.LDAP
{
    public class LDAPUser
    {
        public string? Id { get; set; }
        public int Index { get; set; }
        public string? UserName { get; set; }
        public string? DisplayName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public bool IsDomainUser { get; set; }
        public bool IsActive { get; set; }
        public bool Imported { get; set; }
        public string[]? MemberOf { get; set; }
        public bool Selected { get; set; }
    }
}
