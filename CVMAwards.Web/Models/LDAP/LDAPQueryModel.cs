﻿namespace CVMAwards.Web.Models.LDAP
{
    public class LDAPQueryModel
    {
        public bool ShowExistingUsers { get; set; } = false;
        public string? UserFilter { get; set; }
        public string FilteredGroup { get; set; } = "SelectAGroup";
    }
}
