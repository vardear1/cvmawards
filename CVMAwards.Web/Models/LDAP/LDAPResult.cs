﻿namespace CVMAwards.Web.Models.LDAP
{
    public class LDAPResult
    {
        public LDAPResult()
        {
            Errors = new List<string>();
        }

        public List<string> Errors { get; set; }
        public bool Success { get; set; }
        public int NumberOfUsersImported { get; set; }
        public int TotalUsersAttempted { get; set; }
    }
}
