﻿using CVMAwards.Core.Entities;

namespace CVMAwards.Web.Models.Home
{
    public class AwardDTO: Award
    {
        public string? StatusDesc { get; set; }
        public string? DueDate { get; set; }
        public string? CategoryDesc { get; set; }
        public string? Month { get; set; }
    }
}
